﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Reflection;

namespace Bookmarks
{
    public static class ResourceHelper
    {
        public static Image GetEmbeddedImage(string path)
        {
            var asm = Assembly.GetExecutingAssembly();
            using (var stream = asm.GetManifestResourceStream(path))
            {
                return Image.FromStream(stream);
            }
        }

        public static Icon GetEmbeddedIcon(string path)
        {
            var asm = Assembly.GetExecutingAssembly();
            using (var stream = asm.GetManifestResourceStream(path))
            {
                return new Icon(stream);
            }
        }
    }
}
