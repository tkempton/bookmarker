﻿/// Config.cs
/// Thomas Kempton 2014
///
/// Stores configuration data.
///

namespace Bookmarks.Configuration
{
    using System.Collections.ObjectModel;

    public class Config
    {
        /// <summary>
        /// Gets or sets the browsers.
        /// </summary>
        /// <value>
        /// The browsers.
        /// </value>
        public Collection<Browser> Browsers { get; set; }

        /// <summary>
        /// Gets or sets the websites.
        /// </summary>
        /// <value>
        /// The websites.
        /// </value>
        public Collection<Website> Websites { get; set; }
    }
}
