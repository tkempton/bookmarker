﻿/// Website.cs
/// Thomas Kempton 2014
///
/// Represents a website.
///

namespace Bookmarks.Configuration
{
    using System.Xml.Serialization;

    public class Website
    {
        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        [XmlAttribute]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the URL.
        /// </summary>
        /// <value>
        /// The URL.
        /// </value>
        [XmlAttribute]
        public string Url { get; set; }

        /// <summary>
        /// Gets or sets the default browser.
        /// </summary>
        /// <value>
        /// The default browser.
        /// </value>
        [XmlAttribute]
        public string DefaultBrowser { get; set; }

        /// <summary>
        /// Returns a <see cref="System.String" />
        /// that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String" />
        /// that represents this instance.
        /// </returns>
        public override string ToString()
        {
            return this.Name ?? string.Empty;
        }
    }
}
