﻿/// WebsiteMenuItem.cs
/// Thomas Kempton 2014
///
/// A tool strip menu item that will open a
/// specific link in a specific web browser.
///

namespace Bookmarks
{
    using System.ComponentModel;
    using System.Diagnostics;
    using System.Windows.Forms;
    using Bookmarks.Forms;

    public class WebsiteMenuItem : ToolStripMenuItem
    {
        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="WebsiteMenuItem" /> class.
        /// </summary>
        /// <param name="text">The text.</param>
        /// <param name="browserPath">The browser path.</param>
        /// <param name="url">The URL.</param>
        public WebsiteMenuItem(string text, string browserPath, string url)
        {
            this.Text = text;
            this.BrowserPath = browserPath;
            this.Url = url;

            this.Click += (s, e) => this.OpenBrowser();
        }

        /// <summary>
        /// Gets or sets the browser path.
        /// </summary>
        /// <value>
        /// The browser path.
        /// </value>
        public string BrowserPath { get; set; }

        /// <summary>
        /// Gets or sets the URL.
        /// </summary>
        /// <value>
        /// The URL.
        /// </value>
        public string Url { get; set; }

        /// <summary>
        /// Opens the browser.
        /// </summary>
        private void OpenBrowser()
        {
            if (!string.IsNullOrEmpty(this.BrowserPath) && !string.IsNullOrEmpty(this.Url))
            {
                try
                {

                    Process process = new Process();
                    process.StartInfo.FileName = "\"" + this.BrowserPath + "\"";
                    process.StartInfo.Arguments = "\"" + this.Url + "\"";
                    process.Start();
                }
                catch (Win32Exception)
                {
                    var msg = string.Format(
                            "Browser '{0}' could not be started.  Please verify the file name and path.",
                            this.BrowserPath);
                    using (var window = new ErrorWindow("Browser Error!", msg))
                    {
                        window.ShowDialog();
                    }
                }
            }
        }
    }
}
