﻿namespace Bookmarks.Forms
{
    partial class ConfigurationWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ConfigurationWindow));
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabSites = new System.Windows.Forms.TabPage();
            this.btnDownSite = new System.Windows.Forms.Button();
            this.btnUpSite = new System.Windows.Forms.Button();
            this.btnDelSite = new System.Windows.Forms.Button();
            this.btnModSite = new System.Windows.Forms.Button();
            this.btnNewSite = new System.Windows.Forms.Button();
            this.lstWebsites = new System.Windows.Forms.ListBox();
            this.tabBrowsers = new System.Windows.Forms.TabPage();
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.btnDownBr = new System.Windows.Forms.Button();
            this.btnUpBr = new System.Windows.Forms.Button();
            this.btnDelBr = new System.Windows.Forms.Button();
            this.btnModBr = new System.Windows.Forms.Button();
            this.btnNewBr = new System.Windows.Forms.Button();
            this.lstBrowsers = new System.Windows.Forms.ListBox();
            this.tabControl1.SuspendLayout();
            this.tabSites.SuspendLayout();
            this.tabBrowsers.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabSites);
            this.tabControl1.Controls.Add(this.tabBrowsers);
            this.tabControl1.Location = new System.Drawing.Point(12, 12);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(347, 416);
            this.tabControl1.TabIndex = 0;
            // 
            // tabSites
            // 
            this.tabSites.Controls.Add(this.btnDownSite);
            this.tabSites.Controls.Add(this.btnUpSite);
            this.tabSites.Controls.Add(this.btnDelSite);
            this.tabSites.Controls.Add(this.btnModSite);
            this.tabSites.Controls.Add(this.btnNewSite);
            this.tabSites.Controls.Add(this.lstWebsites);
            this.tabSites.Location = new System.Drawing.Point(4, 25);
            this.tabSites.Name = "tabSites";
            this.tabSites.Padding = new System.Windows.Forms.Padding(3);
            this.tabSites.Size = new System.Drawing.Size(339, 387);
            this.tabSites.TabIndex = 0;
            this.tabSites.Text = "Websites";
            this.tabSites.UseVisualStyleBackColor = true;
            // 
            // btnDownSite
            // 
            this.btnDownSite.Enabled = false;
            this.btnDownSite.Image = ((System.Drawing.Image)(resources.GetObject("btnDownSite.Image")));
            this.btnDownSite.Location = new System.Drawing.Point(297, 48);
            this.btnDownSite.Name = "btnDownSite";
            this.btnDownSite.Size = new System.Drawing.Size(35, 35);
            this.btnDownSite.TabIndex = 5;
            this.toolTip.SetToolTip(this.btnDownSite, "Move Down");
            this.btnDownSite.UseVisualStyleBackColor = true;
            this.btnDownSite.Click += new System.EventHandler(this.btnDownSite_Click);
            // 
            // btnUpSite
            // 
            this.btnUpSite.Enabled = false;
            this.btnUpSite.Image = ((System.Drawing.Image)(resources.GetObject("btnUpSite.Image")));
            this.btnUpSite.Location = new System.Drawing.Point(297, 7);
            this.btnUpSite.Name = "btnUpSite";
            this.btnUpSite.Size = new System.Drawing.Size(35, 35);
            this.btnUpSite.TabIndex = 4;
            this.toolTip.SetToolTip(this.btnUpSite, "Move Up");
            this.btnUpSite.UseVisualStyleBackColor = true;
            this.btnUpSite.Click += new System.EventHandler(this.btnUpSite_Click);
            // 
            // btnDelSite
            // 
            this.btnDelSite.Enabled = false;
            this.btnDelSite.Location = new System.Drawing.Point(201, 355);
            this.btnDelSite.Name = "btnDelSite";
            this.btnDelSite.Size = new System.Drawing.Size(90, 25);
            this.btnDelSite.TabIndex = 3;
            this.btnDelSite.Text = "Delete";
            this.btnDelSite.UseVisualStyleBackColor = true;
            this.btnDelSite.Click += new System.EventHandler(this.btnDelSite_Click);
            // 
            // btnModSite
            // 
            this.btnModSite.Enabled = false;
            this.btnModSite.Location = new System.Drawing.Point(105, 355);
            this.btnModSite.Name = "btnModSite";
            this.btnModSite.Size = new System.Drawing.Size(90, 25);
            this.btnModSite.TabIndex = 2;
            this.btnModSite.Text = "Modify";
            this.btnModSite.UseVisualStyleBackColor = true;
            this.btnModSite.Click += new System.EventHandler(this.btnModSite_Click);
            // 
            // btnNewSite
            // 
            this.btnNewSite.Location = new System.Drawing.Point(9, 355);
            this.btnNewSite.Name = "btnNewSite";
            this.btnNewSite.Size = new System.Drawing.Size(90, 25);
            this.btnNewSite.TabIndex = 1;
            this.btnNewSite.Text = "New";
            this.btnNewSite.UseVisualStyleBackColor = true;
            this.btnNewSite.Click += new System.EventHandler(this.btnNewSite_Click);
            // 
            // lstWebsites
            // 
            this.lstWebsites.FormattingEnabled = true;
            this.lstWebsites.ItemHeight = 16;
            this.lstWebsites.Location = new System.Drawing.Point(7, 7);
            this.lstWebsites.Name = "lstWebsites";
            this.lstWebsites.Size = new System.Drawing.Size(284, 340);
            this.lstWebsites.TabIndex = 0;
            this.lstWebsites.SelectedIndexChanged += new System.EventHandler(this.lstWebsites_SelectedIndexChanged);
            // 
            // tabBrowsers
            // 
            this.tabBrowsers.Controls.Add(this.btnDownBr);
            this.tabBrowsers.Controls.Add(this.btnUpBr);
            this.tabBrowsers.Controls.Add(this.btnDelBr);
            this.tabBrowsers.Controls.Add(this.btnModBr);
            this.tabBrowsers.Controls.Add(this.btnNewBr);
            this.tabBrowsers.Controls.Add(this.lstBrowsers);
            this.tabBrowsers.Location = new System.Drawing.Point(4, 25);
            this.tabBrowsers.Name = "tabBrowsers";
            this.tabBrowsers.Padding = new System.Windows.Forms.Padding(3);
            this.tabBrowsers.Size = new System.Drawing.Size(339, 387);
            this.tabBrowsers.TabIndex = 1;
            this.tabBrowsers.Text = "Browsers";
            this.tabBrowsers.UseVisualStyleBackColor = true;
            // 
            // btnDownBr
            // 
            this.btnDownBr.Enabled = false;
            this.btnDownBr.Image = ((System.Drawing.Image)(resources.GetObject("btnDownBr.Image")));
            this.btnDownBr.Location = new System.Drawing.Point(297, 48);
            this.btnDownBr.Name = "btnDownBr";
            this.btnDownBr.Size = new System.Drawing.Size(35, 35);
            this.btnDownBr.TabIndex = 11;
            this.toolTip.SetToolTip(this.btnDownBr, "Move Down");
            this.btnDownBr.UseVisualStyleBackColor = true;
            this.btnDownBr.Click += new System.EventHandler(this.btnDownBr_Click);
            // 
            // btnUpBr
            // 
            this.btnUpBr.Enabled = false;
            this.btnUpBr.Image = ((System.Drawing.Image)(resources.GetObject("btnUpBr.Image")));
            this.btnUpBr.Location = new System.Drawing.Point(297, 7);
            this.btnUpBr.Name = "btnUpBr";
            this.btnUpBr.Size = new System.Drawing.Size(35, 35);
            this.btnUpBr.TabIndex = 10;
            this.toolTip.SetToolTip(this.btnUpBr, "Move Up");
            this.btnUpBr.UseVisualStyleBackColor = true;
            this.btnUpBr.Click += new System.EventHandler(this.btnUpBr_Click);
            // 
            // btnDelBr
            // 
            this.btnDelBr.Enabled = false;
            this.btnDelBr.Location = new System.Drawing.Point(201, 355);
            this.btnDelBr.Name = "btnDelBr";
            this.btnDelBr.Size = new System.Drawing.Size(90, 25);
            this.btnDelBr.TabIndex = 9;
            this.btnDelBr.Text = "Delete";
            this.btnDelBr.UseVisualStyleBackColor = true;
            this.btnDelBr.Click += new System.EventHandler(this.btnDelBr_Click);
            // 
            // btnModBr
            // 
            this.btnModBr.Enabled = false;
            this.btnModBr.Location = new System.Drawing.Point(105, 355);
            this.btnModBr.Name = "btnModBr";
            this.btnModBr.Size = new System.Drawing.Size(90, 25);
            this.btnModBr.TabIndex = 8;
            this.btnModBr.Text = "Modify";
            this.btnModBr.UseVisualStyleBackColor = true;
            this.btnModBr.Click += new System.EventHandler(this.btnModBr_Click);
            // 
            // btnNewBr
            // 
            this.btnNewBr.Location = new System.Drawing.Point(9, 355);
            this.btnNewBr.Name = "btnNewBr";
            this.btnNewBr.Size = new System.Drawing.Size(90, 25);
            this.btnNewBr.TabIndex = 7;
            this.btnNewBr.Text = "New";
            this.btnNewBr.UseVisualStyleBackColor = true;
            this.btnNewBr.Click += new System.EventHandler(this.btnNewBr_Click);
            // 
            // lstBrowsers
            // 
            this.lstBrowsers.FormattingEnabled = true;
            this.lstBrowsers.ItemHeight = 16;
            this.lstBrowsers.Location = new System.Drawing.Point(7, 7);
            this.lstBrowsers.Name = "lstBrowsers";
            this.lstBrowsers.Size = new System.Drawing.Size(284, 340);
            this.lstBrowsers.TabIndex = 6;
            this.lstBrowsers.SelectedIndexChanged += new System.EventHandler(this.lstBrowsers_SelectedIndexChanged);
            // 
            // ConfigurationWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(369, 438);
            this.Controls.Add(this.tabControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ConfigurationWindow";
            this.Text = "Bookmarker Configuration";
            this.Load += new System.EventHandler(this.ConfigurationWindow_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabSites.ResumeLayout(false);
            this.tabBrowsers.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabSites;
        private System.Windows.Forms.TabPage tabBrowsers;
        private System.Windows.Forms.ListBox lstWebsites;
        private System.Windows.Forms.Button btnDelSite;
        private System.Windows.Forms.Button btnModSite;
        private System.Windows.Forms.Button btnNewSite;
        private System.Windows.Forms.Button btnUpSite;
        private System.Windows.Forms.Button btnDownSite;
        private System.Windows.Forms.ToolTip toolTip;
        private System.Windows.Forms.Button btnDownBr;
        private System.Windows.Forms.Button btnUpBr;
        private System.Windows.Forms.Button btnDelBr;
        private System.Windows.Forms.Button btnModBr;
        private System.Windows.Forms.Button btnNewBr;
        private System.Windows.Forms.ListBox lstBrowsers;
    }
}