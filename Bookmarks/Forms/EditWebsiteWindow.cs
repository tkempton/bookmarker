﻿/// EditWebsiteWindow.cs
/// Thomas Kempton 2014
///
/// Form for editing websites.
///

namespace Bookmarks.Forms
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Windows.Forms;
    using Bookmarks.Configuration;

    public partial class EditWebsiteWindow : Form
    {
        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="EditWebsiteWindow" /> class.
        /// </summary>
        /// <param name="browsers">The browsers.</param>
        public EditWebsiteWindow(IEnumerable<Browser> browsers)
        {
            InitializeComponent();

            this.Browsers = browsers;

            cmbBrowser.Items.AddRange(browsers.ToArray());
            cmbBrowser.SelectedIndex = 0;
        }

        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="EditWebsiteWindow" /> class.
        /// </summary>
        /// <param name="site">The site.</param>
        /// <param name="browsers">The browsers.</param>
        public EditWebsiteWindow(Website site, IEnumerable<Browser> browsers) : this(browsers)
        {
            txtName.Text = site.Name;
            txtUrl.Text = site.Url;

            var br = this.Browsers.FirstOrDefault(b => b.Id == site.DefaultBrowser);

            if (br != null)
            {
                cmbBrowser.SelectedItem = br;
            }
            else
            {
                cmbBrowser.SelectedIndex = 0;
            }
        }

        /// <summary>
        /// Gets the website.
        /// </summary>
        /// <value>
        /// The website.
        /// </value>
        public Website Website { get; private set; }

        /// <summary>
        /// Gets or sets the browsers.
        /// </summary>
        /// <value>
        /// The browsers.
        /// </value>
        private IEnumerable<Browser> Browsers { get; set; }

        /// <summary>
        /// Handles the Click event of the btnSave control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">
        ///   The <see cref="EventArgs" /> instance containing the event data.
        /// </param>
        private void btnSave_Click(object sender, EventArgs e)
        {
            this.Website = new Website()
            {
                Name = txtName.Text,
                Url = txtUrl.Text
            };

            var browser = cmbBrowser.SelectedItem as Browser;
            if (browser != null)
            {
                this.Website.DefaultBrowser = browser.Id;
            }

            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        /// <summary>
        /// Handles the Click event of the btnCancel control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">
        ///   The <see cref="EventArgs" /> instance containing the event data.
        /// </param>
        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        /// <summary>
        /// Handles the TextChanged event of the txtName control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">
        ///   The <see cref="EventArgs" /> instance containing the event data.
        /// </param>
        private void txt_TextChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(txtName.Text) && !string.IsNullOrWhiteSpace(txtUrl.Text))
            {
                btnSave.Enabled = true;
            }
            else
            {
                btnSave.Enabled = false;
            }
        }
    }
}