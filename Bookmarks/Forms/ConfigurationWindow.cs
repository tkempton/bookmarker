﻿/// ConfigurationWindow.cs
/// Thomas Kempton 2014
///
/// Form for managing bookmarks and browsers.
///

namespace Bookmarks.Forms
{
    using System;
    using System.Linq;
    using System.Windows.Forms;
    using Bookmarks.Configuration;
    using System.ComponentModel;

    public partial class ConfigurationWindow : Form
    {
        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="ConfigurationWindow" /> class.
        /// </summary>
        /// <param name="config">The config.</param>
        public ConfigurationWindow(Config config)
        {
            this.Configuration = config;
            InitializeComponent();
        }

        /// <summary>
        /// Gets the configuration.
        /// </summary>
        /// <value>
        /// The configuration.
        /// </value>
        public Config Configuration { get; private set; }

        /// <summary>
        /// Gets or sets the on changed.
        /// </summary>
        /// <value>
        /// The on changed.
        /// </value>
        public EventHandler OnChanged { get; set; }

        /// <summary>
        /// Saves the config.
        /// </summary>
        private void SaveConfig()
        {
            var bg = new BackgroundWorker();
            bg.DoWork += (s, e) =>
            {
                this.Configuration.Browsers.Clear();
                foreach (Browser browser in lstBrowsers.Items)
                {
                    this.Configuration.Browsers.Add(browser);
                }

                this.Configuration.Websites.Clear();
                foreach (Website website in lstWebsites.Items)
                {
                    this.Configuration.Websites.Add(website);
                }

                if (this.OnChanged != null)
                {
                    this.OnChanged(this, EventArgs.Empty);
                }
            };

            bg.RunWorkerAsync();
        }

        /// <summary>
        /// Handles the Load event of the ConfigurationWindow control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">
        ///   The <see cref="EventArgs" /> instance containing the event data.
        /// </param>
        private void ConfigurationWindow_Load(object sender, EventArgs e)
        {
            lstWebsites.Items.AddRange(this.Configuration.Websites.ToArray());
            lstBrowsers.Items.AddRange(this.Configuration.Browsers.ToArray());
        }

        /// <summary>
        /// Handles the Click event of the btnNewSite control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">
        ///   The <see cref="EventArgs" /> instance containing the event data.
        /// </param>
        private void btnNewSite_Click(object sender, EventArgs e)
        {
            if (this.Configuration.Browsers.Any())
            {
                var form = new EditWebsiteWindow(this.Configuration.Browsers);
                form.Text = "New Website";

                if (form.ShowDialog() == DialogResult.OK)
                {
                    lstWebsites.Items.Add(form.Website);
                    this.SaveConfig();
                }
            }
            else
            {
                using (var window = new ErrorWindow("Configuration Error!", "You must add a browser before you can add a website."))
                {
                    window.ShowDialog();
                }
            }
        }

        /// <summary>
        /// Handles the Click event of the btnModSite control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">
        ///   The <see cref="EventArgs" /> instance containing the event data.
        /// </param>
        private void btnModSite_Click(object sender, EventArgs e)
        {
            if (this.Configuration.Browsers.Any())
            {
                var site = lstWebsites.SelectedItem as Website;
                var form = new EditWebsiteWindow(site, this.Configuration.Browsers);
                form.Text = "Modify Website";

                if (form.ShowDialog() == DialogResult.OK)
                {
                    var index = lstWebsites.Items.IndexOf(site);
                    lstWebsites.Items.RemoveAt(index);
                    lstWebsites.Items.Insert(index, form.Website);
                    lstWebsites.SelectedIndex = index;

                    this.SaveConfig();
                }
            }
            else
            {
                using (var window = new ErrorWindow("Configuration Error!", "You must add a browser before you can edit a website."))
                {
                    window.ShowDialog();
                }
            }
        }

        /// <summary>
        /// Handles the Click event of the btnDelSite control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">
        ///   The <see cref="EventArgs" /> instance containing the event data.
        /// </param>
        private void btnDelSite_Click(object sender, EventArgs e)
        {
            var site = lstWebsites.SelectedItem as Website;
            var result = MessageBox.Show(
                    string.Format("Are you sure you want to delete {0} ?", site.Name),
                    "Delete Website",
                    MessageBoxButtons.YesNo);

            if (result == DialogResult.Yes)
            {
                lstWebsites.Items.Remove(site);
                this.SaveConfig();
            }
        }

        /// <summary>
        /// Handles the Click event of the btnUpSite control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">
        ///   The <see cref="EventArgs" /> instance containing the event data.
        /// </param>
        private void btnUpSite_Click(object sender, EventArgs e)
        {
            var index = lstWebsites.SelectedIndex;

            if (index != 0)
            {
                var site = lstWebsites.SelectedItem as Website;
                lstWebsites.Items.RemoveAt(index);
                lstWebsites.Items.Insert(--index, site);
                lstWebsites.SelectedIndex = index;

                this.SaveConfig();
            }
        }

        /// <summary>
        /// Handles the Click event of the btnDownSite control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">
        ///   The <see cref="EventArgs" /> instance containing the event data.
        /// </param>
        private void btnDownSite_Click(object sender, EventArgs e)
        {
            var index = lstWebsites.SelectedIndex;

            if (index < lstWebsites.Items.Count - 1)
            {
                var site = lstWebsites.SelectedItem as Website;
                lstWebsites.Items.RemoveAt(index);
                lstWebsites.Items.Insert(++index, site);
                lstWebsites.SelectedIndex = index;

                this.SaveConfig();
            }
        }

        /// <summary>
        /// Handles the SelectedIndexChanged event of the lstWebsites control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">
        ///   The <see cref="EventArgs" /> instance containing the event data.
        /// </param>
        private void lstWebsites_SelectedIndexChanged(object sender, EventArgs e)
        {
            var site = lstWebsites.SelectedItem as Website;

            if (site != null)
            {
                btnModSite.Enabled = true;
                btnDelSite.Enabled = true;
                btnUpSite.Enabled = true;
                btnDownSite.Enabled = true;
            }
            else
            {
                btnModSite.Enabled = false;
                btnDelSite.Enabled = false;
                btnUpSite.Enabled = false;
                btnDownSite.Enabled = false;
            }
        }

        /// <summary>
        /// Handles the Click event of the btnNewBr control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">
        ///   The <see cref="EventArgs" /> instance containing the event data.
        /// </param>
        private void btnNewBr_Click(object sender, EventArgs e)
        {
            var form = new EditBrowserWindow();
            form.Text = "New Browser";

            if (form.ShowDialog() == DialogResult.OK)
            {
                lstBrowsers.Items.Add(form.Browser);
                this.SaveConfig();
            }
        }

        /// <summary>
        /// Handles the Click event of the btnModBr control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">
        ///   The <see cref="EventArgs" /> instance containing the event data.
        /// </param>
        private void btnModBr_Click(object sender, EventArgs e)
        {
            var browser = lstBrowsers.SelectedItem as Browser;
            var form = new EditBrowserWindow(browser);
            form.Text = "Modify Browser";

            if (form.ShowDialog() == DialogResult.OK)
            {
                var index = lstBrowsers.Items.IndexOf(browser);
                lstBrowsers.Items.RemoveAt(index);
                lstBrowsers.Items.Insert(index, form.Browser);
                lstBrowsers.SelectedIndex = index;

                this.SaveConfig();
            }
        }

        /// <summary>
        /// Handles the Click event of the btnDelBr control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">
        ///   The <see cref="EventArgs" /> instance containing the event data.
        /// </param>
        private void btnDelBr_Click(object sender, EventArgs e)
        {
            var browser = lstBrowsers.SelectedItem as Browser;
            var result = MessageBox.Show(
                    string.Format("Are you sure you want to delete {0} ?", browser.Name),
                    "Delete Browser",
                    MessageBoxButtons.YesNo);

            if (result == DialogResult.Yes)
            {
                lstBrowsers.Items.Remove(browser);
                this.SaveConfig();
            }
        }

        /// <summary>
        /// Handles the Click event of the btnUpBr control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">
        ///   The <see cref="EventArgs" /> instance containing the event data.
        /// </param>
        private void btnUpBr_Click(object sender, EventArgs e)
        {
            var index = lstBrowsers.SelectedIndex;

            if (index != 0)
            {
                var browser = lstBrowsers.SelectedItem as Browser;
                lstBrowsers.Items.RemoveAt(index);
                lstBrowsers.Items.Insert(--index, browser);
                lstBrowsers.SelectedIndex = index;

                this.SaveConfig();
            }
        }

        /// <summary>
        /// Handles the Click event of the btnDownBr control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">
        ///   The <see cref="EventArgs" /> instance containing the event data.
        /// </param>
        private void btnDownBr_Click(object sender, EventArgs e)
        {
            var index = lstBrowsers.SelectedIndex;

            if (index < lstBrowsers.Items.Count - 1)
            {
                var browser = lstBrowsers.SelectedItem as Browser;
                lstBrowsers.Items.RemoveAt(index);
                lstBrowsers.Items.Insert(++index, browser);
                lstBrowsers.SelectedIndex = index;

                this.SaveConfig();
            }
        }

        /// <summary>
        /// Handles the SelectedIndexChanged event of the lstBrowsers control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">
        ///   The <see cref="EventArgs" /> instance containing the event data.
        /// </param>
        private void lstBrowsers_SelectedIndexChanged(object sender, EventArgs e)
        {
            var browser = lstBrowsers.SelectedItem as Browser;

            if (browser != null)
            {
                btnModBr.Enabled = true;
                btnDelBr.Enabled = true;
                btnUpBr.Enabled = true;
                btnDownBr.Enabled = true;
            }
            else
            {
                btnModBr.Enabled = false;
                btnDelBr.Enabled = false;
                btnUpBr.Enabled = false;
                btnDownBr.Enabled = false;
            }
        }
    }
}
