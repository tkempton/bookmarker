﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Bookmarks.Forms
{
    public partial class ErrorWindow : Form
    {
        public ErrorWindow(string title, string message)
        {
            InitializeComponent();
            this.Text = title;
            this.lblError.Text = message;
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
