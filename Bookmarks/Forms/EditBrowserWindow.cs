﻿/// EditBrowserWindow.cs
/// Thomas Kempton 2014
///
/// Form for editing browsers.
///

namespace Bookmarks.Forms
{
    using System;
    using System.Windows.Forms;
    using Bookmarks.Configuration;

    public partial class EditBrowserWindow : Form
    {
        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="EditBrowserWindow" /> class.
        /// </summary>
        public EditBrowserWindow()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="EditBrowserWindow" /> class.
        /// </summary>
        /// <param name="browser">The browser.</param>
        public EditBrowserWindow(Browser browser) : this()
        {
            txtName.Text = browser.Name;
            txtId.Text = browser.Id;
            txtIcon.Text = browser.Icon;
            txtPath.Text = browser.Path;
        }

        /// <summary>
        /// Gets the browser.
        /// </summary>
        /// <value>
        /// The browser.
        /// </value>
        public Browser Browser { get; private set; }

        /// <summary>
        /// Handles the Click event of the btnSave control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">
        ///   The <see cref="EventArgs" /> instance containing the event data.
        /// </param>
        private void btnSave_Click(object sender, EventArgs e)
        {
            this.Browser = new Browser()
            {
                Name = txtName.Text,
                Id = txtId.Text,
                Icon = txtIcon.Text,
                Path = txtPath.Text
            };

            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        /// <summary>
        /// Handles the Click event of the btnCancel control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">
        ///   The <see cref="EventArgs" /> instance containing the event data.
        /// </param>
        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        /// <summary>
        /// Handles the TextChanged event of the txtName control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">
        ///   The <see cref="EventArgs" /> instance containing the event data.
        /// </param>
        private void txt_TextChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(txtName.Text)
                && !string.IsNullOrWhiteSpace(txtId.Text)
                && !string.IsNullOrWhiteSpace(txtIcon.Text)
                && !string.IsNullOrWhiteSpace(txtPath.Text))
            {
                btnSave.Enabled = true;
            }
            else
            {
                btnSave.Enabled = false;
            }
        }
    }
}
