﻿/// BookmarksContext.cs
/// Thomas Kempton 2014
///
/// Bookmarker application context.
///

namespace Bookmarks
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Drawing;
    using System.IO;
    using System.Linq;
    using System.Reflection;
    using System.Windows.Forms;
    using Bookmarks.Configuration;
    using Bookmarks.Forms;

    class BoomarksContext : ApplicationContext
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="BoomarksContext" /> class.
        /// </summary>
        public BoomarksContext()
        {
            this.NotificationIcon = new NotifyIcon();
            this.Menu = new ContextMenuStrip();
            this.NotificationIcon.ContextMenuStrip = this.Menu;
            this.NotificationIcon.Icon = ResourceHelper.GetEmbeddedIcon("Bookmarks.Resources.book.ico");
            this.NotificationIcon.Text = "Bookmarker";
            this.NotificationIcon.MouseUp += NotificationIcon_MouseUp;
            this.NotificationIcon.Visible = true;
        }

        /// <summary>
        /// Gets or sets the icon.
        /// </summary>
        /// <value>
        /// The icon.
        /// </value>
        private NotifyIcon NotificationIcon { get; set; }

        /// <summary>
        /// Gets or sets the menu.
        /// </summary>
        /// <value>
        /// The menu.
        /// </value>
        private ContextMenuStrip Menu { get; set; }

        /// <summary>
        /// Gets or sets the configuration.
        /// </summary>
        /// <value>
        /// The configuration.
        /// </value>
        private Config Configuration { get; set; }

        /// <summary>
        /// Gets or sets the config window.
        /// </summary>
        /// <value>
        /// The config window.
        /// </value>
        private ConfigurationWindow ConfigWindow { get; set; }

        /// <summary>
        /// Gets or sets the embedded icons.
        /// </summary>
        /// <value>
        /// The embedded icons.
        /// </value>
        private static Dictionary<string, string> EmbeddedIcons =
            new Dictionary<string, string>()
            {
                { "ie", "Bookmarks.Resources.ie.png" },
                { "ff", "Bookmarks.Resources.firefox.png" },
                { "ch", "Bookmarks.Resources.chrome.png" }
            };

        /// <summary>
        /// Setups this instance.
        /// </summary>
        /// <returns></returns>
        public bool Setup()
        {
            if (this.LoadConfig())
            {
                this.PopulateMenu();
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Releases the unmanaged resources used by the
        /// <see cref="T:System.Windows.Forms.ApplicationContext" />
        /// and optionally releases the managed resources.
        /// </summary>
        /// <param name="disposing">
        ///   true to release both managed and unmanaged resources;
        ///   false to release only unmanaged resources.
        /// </param>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                this.NotificationIcon.Dispose();
            }

            base.Dispose(disposing);
        }

        /// <summary>
        /// Handles the MouseUp event of the NotificationIcon control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="MouseEventArgs" /> instance containing the event data.</param>
        private void NotificationIcon_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                var method = typeof(NotifyIcon).GetMethod("ShowContextMenu", BindingFlags.Instance | BindingFlags.NonPublic);
                method.Invoke(this.NotificationIcon, null);
            }
        }

        /// <summary>
        /// Populates the context menu.
        /// </summary>
        private void PopulateMenu()
        {
            if (this.Configuration.Browsers.Any() && this.Configuration.Websites.Any())
            {
                // create a menu item for each website.
                foreach (var site in this.Configuration.Websites.ToList())
                {
                    // create a sub-item for each browser.
                    var subs = new Collection<ToolStripItem>();
                    foreach (var browser in this.Configuration.Browsers.ToList())
                    {
                        var br = new WebsiteMenuItem(browser.Name, browser.Path, site.Url);
                        br.Image = this.GetImage(browser.Icon);
                        subs.Add(br);
                    }

                    var item = new WebsiteMenuItem(site.Name, null, site.Url);
                    item.DropDownItems.AddRange(subs.ToArray());

                    // setup default browser link
                    if (!string.IsNullOrEmpty(site.DefaultBrowser))
                    {
                        var browser = this.Configuration.Browsers.FirstOrDefault(b => string.Compare(b.Id, site.DefaultBrowser, true) == 0);

                        if (browser != null)
                        {
                            item.BrowserPath = browser.Path;
                            item.Image = this.GetImage(browser.Icon);
                        }
                    }

                    this.Menu.Items.Add(item);
                }

                this.Menu.Items.Add(new ToolStripSeparator());
            }

            // static menu items
            this.Menu.Items.Add(new ToolStripMenuItem("Configuration", null, (s, e) => this.ShowConfigurationWindow()));
            this.Menu.Items.Add(new ToolStripSeparator());
            this.Menu.Items.Add(new ToolStripMenuItem("Close", null, (s, e) => this.ExitThread()));
        }

        /// <summary>
        /// Gets the image.
        /// </summary>
        /// <param name="filename">The filename.</param>
        /// <returns>The image.</returns>
        private Image GetImage(string path)
        {
            if (EmbeddedIcons.ContainsKey(path))
            {
                return ResourceHelper.GetEmbeddedImage(EmbeddedIcons[path]);
            }
            else
            {
                var iconPath = Path.Combine(Directory.GetCurrentDirectory(), path);

                try
                {
                    return Bitmap.FromFile(iconPath);
                }
                catch
                {
                    return SystemIcons.Warning.ToBitmap();
                }
            }
        }

        /// <summary>
        /// Shows the configuration window.
        /// </summary>
        private void ShowConfigurationWindow()
        {
            if (this.ConfigWindow != null && this.ConfigWindow.Visible)
            {
                this.ConfigWindow.Focus();
            }
            else
            {
                if (this.ConfigWindow != null && !this.ConfigWindow.IsDisposed)
                {
                    this.ConfigWindow.Dispose();
                }

                this.ConfigWindow = new ConfigurationWindow(this.Configuration);
                this.ConfigWindow.OnChanged += (s, e) =>
                {
                    this.Configuration = this.ConfigWindow.Configuration;
                    this.SaveConfig();

                    this.Menu.Invoke(new MethodInvoker(() =>
                    {
                        this.Menu.Items.Clear();
                        this.PopulateMenu();
                    }));
                };

                this.ConfigWindow.ShowDialog();
            }
        }

        /// <summary>
        /// Loads the config.
        /// </summary>
        private bool LoadConfig()
        {
            if (!File.Exists("config.xml"))
            {
                // create dummy/default config file
                this.Configuration = new Config();

                this.Configuration.Browsers = new Collection<Browser>()
                {
                    new Browser() { Id = "ff", Name = "Firefox", Path = @"firefox.exe", Icon = "ff" },
                    new Browser() { Id = "ch", Name = "Chrome", Path = @"chrome.exe", Icon = "ch" },
                    new Browser() { Id = "ie", Name = "Internet Explorer", Path = @"iexplore.exe", Icon = "ie" }
                };

                this.Configuration.Websites = new Collection<Website>()
                {
                    new Website() { Name = "Google", Url = "www.google.ca", DefaultBrowser = "ff" },
                    new Website() { Name = "Facebook", Url = "www.facebook.com", DefaultBrowser = "ch" },
                    new Website() { Name = "Youtube", Url = "www.youtube.com", DefaultBrowser = "ie" }
                };

                this.SaveConfig();
            }
            else
            {
                try
                {
                    // read existing config file
                    using (var stream = new FileStream("config.xml", FileMode.Open))
                    using (var reader = new StreamReader(stream))
                    {
                        var xml = reader.ReadToEnd();
                        this.Configuration = XmlHelper.Deserialize<Config>(xml);
                    }
                }
                catch (InvalidOperationException)
                {
                    var fileInfo = new FileInfo("config.xml");
                    var msg = string.Format("Config file '{0}' is not valid.", fileInfo.FullName);
                    using (var window = new ErrorWindow("Config File Error!", msg))
                    {
                        window.ShowDialog();
                        return false;
                    }
                }
            }

            return true;
        }

        /// <summary>
        /// Saves the config.
        /// </summary>
        private void SaveConfig()
        {
            using (var stream = new FileStream("config.xml", FileMode.Create))
            using (var writer = new StreamWriter(stream))
            {
                var xml = XmlHelper.Serialize(this.Configuration);
                writer.WriteLine(xml);
            }
        }

        /// <summary>
        /// Application main line entry point.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            var context = new BoomarksContext();

            if (context.Setup())
            {
                Application.Run(context);
            }
        }
    }
}