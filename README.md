# README #

### What does this app do? ###

This application runs as a NotificationIcon, and allows you to bookmark websites outside of a specific browser.

### How do I get set up? ###

Download the executable from the Downloads sections, or download the source and compile it using VS2010.

It will auto-generate its configuration file the first time it is run.

I recommend throwing it somewhere like 'C:\Bookmarks\' so it will be out of the way and easy to find later.

### I find this so useful, I want it to start when I boot up my PC! ###

In Windows 7, just copy a shortcut to the application executable into 'C:\Users\[username]\AppData\Roaming\Microsoft\Windows\Start Menu\Programs\Startup'

In other versions of Windows, consult Google.

### Something is broken! ###

File a bug!  Or if you know me IRL, contact me anyway you know how (email, in person, carrier pigeon, etc).